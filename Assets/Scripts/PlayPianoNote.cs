﻿using UnityEngine;
using System.Collections;

public class PlayPianoNote : MonoBehaviour {

    public AudioSource[] notes;

    private int _soundIndex;
                    

    public void PlayNote()
    {
        notes[(_soundIndex + 1) % notes.Length].Play();
        _soundIndex++;
    }
}
