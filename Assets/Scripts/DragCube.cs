﻿using UnityEngine;
using System.Collections;

public class DragCube : MonoBehaviour {

    public bool constrainX = false;
    public bool constrainY = false;
    public bool constrainZ = false;

    public Transform[] limits;

    public GameObject character;

    private Vector3 deltaPosition;
    private int fingerIndex;

    private MeshRenderer _render;
    private Color _originalColor;

    void Awake()
    {
        _render = GetComponent<MeshRenderer>();
        _originalColor = _render.material.color;
    }

    void OnEnable()
    {
        EasyTouch.On_Drag += On_Drag;
        EasyTouch.On_DragStart += On_DragStart;
        EasyTouch.On_DragEnd += On_DragEnd;
    }

    void OnDisable()
    {
        UnsubscribeEvent();
    }

    void OnDestroy()
    {
        UnsubscribeEvent();
    }

    void UnsubscribeEvent()
    {
        EasyTouch.On_Drag -= On_Drag;
        EasyTouch.On_DragStart -= On_DragStart;
        EasyTouch.On_DragEnd -= On_DragEnd;
    }

    // At the drag beginning 
    void On_DragStart(Gesture gesture)
    {
        // Verification that the action on the object
        if (gesture.pickedObject == gameObject)
        {
            fingerIndex = gesture.fingerIndex;
            _render.material.color = Color.yellow;

            // the world coordinate from touch
            Vector3 position = gesture.GetTouchToWorldPoint(gesture.pickedObject.transform.position);
            deltaPosition = position - transform.position;

            
            if (Vector3.Distance(transform.position, character.transform.position) < 2f)
            {
                character.transform.parent = transform;
                var navMesh = character.gameObject.GetComponent<NavMeshAgent>();
                navMesh.enabled = false;
            }
        }
    }

    // During the drag
    void On_Drag(Gesture gesture)
    {

        // Verification that the action on the object
        if (gesture.pickedObject == gameObject && fingerIndex == gesture.fingerIndex)
        {

            // the world coordinate from touch
            Vector3 position = gesture.GetTouchToWorldPoint(gesture.pickedObject.transform.position);
            position = position - deltaPosition;

            position.x = Mathf.Clamp(position.x, limits[0].position.x, limits[1].position.x);
            position.y = Mathf.Clamp(position.y, limits[0].position.y, limits[1].position.y);
            position.z = Mathf.Clamp(position.z, limits[0].position.z, limits[1].position.z);

            transform.position = position;


            // QUe el personaje siga a la plataforma, hay que hacerlo con navmesh
        }
    }

    // At the drag end
    void On_DragEnd(Gesture gesture)
    {

        // Verification that the action on the object
        if (gesture.pickedObject == gameObject)
        {
            _render.material.color = _originalColor;
            // Cuando termina de hacer el drag :3
            character.transform.parent = null;
            var navMesh = character.gameObject.GetComponent<NavMeshAgent>();
            navMesh.enabled = true;
        }
    }

    void OnTriggerEnter(Collider c)
    {
        c.gameObject.transform.position -= Vector3.up*2;
    }

    void OnTriggerExit(Collider c)
    {
        c.gameObject.transform.position += Vector3.up * 2;
    }
}
