﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public static LevelManager instance;

    public SceneID firstScene = SceneID.LEVEL1;

    public GameObject loadingScreen;

    public Text txtLoading;

    private int _actualScene = 0;
    private int _sceneToLoad;

    private bool _autoSkip = true;


    void Awake()
    {
        instance = this;
        GameObject.DontDestroyOnLoad(this);
        _sceneToLoad = (int)firstScene;
    }


    void Update()
    {
        if (_sceneToLoad != 0)
        {
            StartCoroutine(LoadAsync(_sceneToLoad));

            _sceneToLoad = 0;
        }
    }


    public void LoadScene(SceneID scene, bool skipkey = false)
    {
        _sceneToLoad = (int)scene;

        _autoSkip = skipkey;
    }


    IEnumerator LoadAsync(int scene)
    {
        yield return null;

        if (_actualScene != 0) SceneManager.UnloadScene(_actualScene);

        txtLoading.text = "Cargando...";
        loadingScreen.SetActive(true);

        AsyncOperation ao = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Single);

        ao.allowSceneActivation = false;

        while (!ao.isDone)
        {
            float progress = Mathf.Clamp01(ao.progress / 0.9f);
            print("Loading progress: " + (progress * 100) + "%");


            if (ao.progress == 0.9f)
            {
                txtLoading.text = "Pulse una tecla para continuar";
                if (_autoSkip || Input.anyKeyDown)
                {
                    loadingScreen.SetActive(false);

                    ao.allowSceneActivation = true;
                    _autoSkip = false;
                    _actualScene = scene; 
                }
            }

            yield return null;
        }
    }
}

public enum SceneID
{
    MENU = 1,
    LEVEL1,
    LEVEL2
}