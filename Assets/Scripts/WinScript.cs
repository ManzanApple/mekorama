﻿using UnityEngine;
using System.Collections;

public class WinScript : MonoBehaviour {

    void OnTriggerEnter(Collider c)
    {
        if (LevelManager.instance) LevelManager.instance.LoadScene(SceneID.MENU, true);
    }
}
