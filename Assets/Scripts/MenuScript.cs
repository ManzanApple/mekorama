﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {

    public void LoadLevel1()
    {
        CheckAndLoadLevel(SceneID.LEVEL1);
    }

    public void LoadLevel2()
    {
        CheckAndLoadLevel(SceneID.LEVEL2);
    }

    void CheckAndLoadLevel(SceneID scene)
    {
        if (LevelManager.instance) LevelManager.instance.LoadScene(scene, true);
    }
}
