﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public float rotationSpeed = 5f;

    public Camera mainCamera;

    public float originalSize = 10f;

    private Quaternion _targetRotation;
    private bool _isStarting = true;

    void Awake()
    {
        _targetRotation = transform.rotation;
    }

    void Update()
    {
        if (_isStarting) StartMove();
    }

    void StartMove()
    {
        if (mainCamera.orthographicSize >= originalSize)
            mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, originalSize, 0.1f);
        else
            _isStarting = false;
    }

	void LateUpdate()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, _targetRotation, rotationSpeed * Time.deltaTime);
    }

    void On_SwipeEnd(Gesture gesture)
    {
        if (gesture.swipe == EasyTouch.SwipeDirection.Right)
        {
            _targetRotation *= Quaternion.Euler(0, 90, 0);
        }
        else if (gesture.swipe == EasyTouch.SwipeDirection.Left)
        {
            _targetRotation *= Quaternion.Euler(0, -90, 0);
        }
    }

    void OnEnable()
    {
        EasyTouch.On_SwipeEnd += On_SwipeEnd;
    }

    void OnDisable()
    {
        EasyTouch.On_SwipeEnd -= On_SwipeEnd;
    }

    void OnDestroy()
    {
        EasyTouch.On_SwipeEnd -= On_SwipeEnd;
    }
}
