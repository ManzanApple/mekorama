﻿using UnityEngine;
using System.Collections;

public class PauseScript : MonoBehaviour {

    public GameObject pauseButton;
    public GameObject pausedMenu;

    public AudioSource backgroundMusic;

    private AudioSource _pauseMusic;

    public void Awake()
    {
        _pauseMusic = GetComponent<AudioSource>();
    }

    public void Pause()
    {
        pausedMenu.SetActive(true);
        pauseButton.SetActive(false);
        backgroundMusic.volume = 0;
        _pauseMusic.volume = 0.7f;
    }

    public void Unpause()
    {
        pausedMenu.SetActive(false);
        pauseButton.SetActive(true);
        backgroundMusic.volume = 0.7f;
        _pauseMusic.volume = 0;
    }

    public void GoToMenu()
    {
        if (LevelManager.instance) LevelManager.instance.LoadScene(SceneID.MENU, true);
    }
}
