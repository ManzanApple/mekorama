﻿using UnityEngine;
using System.Collections;

public class CharacterMove : MonoBehaviour {

    public GameObject cam;

    public GameObject badClick;
    public GameObject goodClick;

    public PlayPianoNote piano;

    private NavMeshAgent _nav;
    private GameObject _lastFeedback;

    void Awake ()
    {
        _nav         = gameObject.GetComponent<NavMeshAgent>();
    }

    void On_SimpleTap(Gesture gesture)
    {

        var tapPos = gesture.GetTouchToWorldPoint(1);

        RaycastHit rHit;

        if (Physics.Raycast(tapPos, cam.transform.forward, out rHit))
        {
            // Si el raycast es sobre si mismo no hace nada
            if (rHit.collider.gameObject == gameObject) return;


            NavMeshPath path = new NavMeshPath();

            if (NavMesh.CalculatePath(transform.position, rHit.transform.position, NavMesh.AllAreas, path))
            {
                if (_lastFeedback) Destroy(_lastFeedback);

                if (path.status == NavMeshPathStatus.PathPartial)
                {
                    _lastFeedback = (GameObject)Instantiate(badClick, rHit.transform.position + Vector3.up, Quaternion.identity);
                }
                else
                {
                    if (piano) piano.PlayNote();
                    _nav.SetDestination(rHit.transform.position);
                    _lastFeedback = (GameObject)Instantiate(goodClick, rHit.transform.position + Vector3.up, Quaternion.identity);
                }
            }
        }
    }

    void OnEnable()
    {
        EasyTouch.On_SimpleTap += On_SimpleTap;
    }

    void OnDisable()
    {
        EasyTouch.On_SimpleTap -= On_SimpleTap;
    }

    void OnDestroy()
    {
        EasyTouch.On_SimpleTap -= On_SimpleTap;
    }
}
